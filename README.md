## A Haskell interface to Rosie Pattern Language

### Installation

Assuming you have already installed ghc and cabal...

1. Install rosie, which will put `librosie` into a standard place (e.g. `/usr/local/lib`).

2. (Linux only) Install libnuma, e.g. `apt-get install libnuma-dev`

3. Run `make` in this haskell directory

4. Optionally, run the tests using either `make test` or `cabal new-run Rosie`

### Using Rosie.hs

Currently, there is no documentation for `Rosie.hs`, but there are tests defined
that illustrate how this interface to `librosie` works.

You need to have Rosie (and Haskell) installed.

### Thanks

Many thanks to the author of this Haskell binding, [Laurence Emms](https://gitlab.com/lemms)!

