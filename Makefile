## -*- Mode: Makefile; -*-                                             
##
## LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
## AUTHORS: Laurence Emms, Jamie A. Jennings

REPORTED_PLATFORM=$(shell (uname -o || uname -s) 2> /dev/null)
ifeq ($(REPORTED_PLATFORM), Darwin)
PLATFORM=macosx
else ifeq ($(REPORTED_PLATFORM), GNU/Linux)
PLATFORM=linux
else
PLATFORM=none
endif

default: Rosie

haskell_installed=$(shell command -v "cabal")
rosie_installed=$(shell command -v "rosie")

.PHONY:
dependencies:
	@if [ -z "$(haskell_installed)" ]; then \
		echo Haskell not installed \(cannot find \'cabal\' in PATH\); \
		false; \
	fi;
	@if [ -z "$(rosie_installed)" ]; then \
		echo Rosie not installed \(cannot find \'rosie\' in PATH\); \
		false; \
	fi;

Rosie: Rosie.hs dependencies
	cabal new-install --only-dependencies Rosie
	cabal new-build Rosie

test: dependencies
	cabal new-run Rosie

clean:
	$(RM) Rosie.hi Rosie.o
	rm -rf dist dist-newstyle

echo:
	@echo "HOME= $(HOME)"
	@echo "PLAT= $(PLAT)"
	@echo "CC= $(CC)"
	@echo "CFLAGS= $(CFLAGS)"
	@echo "LDFLAGS= $(LDFLAGS)"
	@echo "LIBS= $(LIBS)"
	@echo "RM= $(RM)"

## Targets that do not create files
.PHONY: default clean echo
